#let vec-neg(v) = v.map(v => -v)
#let vec-add(v0, v1) = v0.zip(v1).map(((a, b)) => a + b)
#let vec-sub(v0, v1) = v0.zip(v1).map(((a, b)) => a - b)
#let vec-mul(v, s) = v.map(v => s * v)
#let vec-elemprod(v0, v1) = v0.zip(v1).map(((a, b)) => a * b)
#let vec-div(v, s) = v.map(v => v / s)
#let vec-dot(v0, v1) = v0.zip(v1).map(((a, b)) => a * b).sum()
#let vec-cross((v0x, v0y, v0z), (v1x, v1y, v1z)) = (
    v0y * v1z - v0z * v1y,
    v0z * v1x - v0x * v1z,
    v0x * v1y - v0y * v1x,
)
#let vec-unit(v) = vec-div(v, calc.sqrt(vec-dot(v, v)))
#let vec-2to3((x, y)) = (x, y, 0)

#let quat(..rot) = {
    let rot = rot.named()
    let (ax, ay, az) = vec-unit(rot.axis)
    let c = calc.cos(rot.angle / 2); let s = calc.sin(rot.angle / 2)
    return (
        c,
        ax * s,
        ay * s,
        az * s,
    )
}
#let quat-mul(q0, q1) = {
    let (a1, b1, c1, d1) = q0
    let (a2, b2, c2, d2) = q1
    return (
        a1 * a2 - b1 * b2 - c1 * c2 - d1 * d2,
        a1 * b2 + b1 * a2 + c1 * d2 - d1 * c2,
        a1 * c2 - b1 * d2 + c1 * a2 + d1 * b2,
        a1 * d2 + b1 * c2 - c1 * b2 + d1 * a2,
    )
}
#let quat-conj((w, x, y, z)) = (
     w,
    -x,
    -y,
    -z,
)
#let quat-rot(q, (x, y, z)) = {
    let v = (0, x, y, z)
    quat-mul(
        quat-mul(
            q,
            v
        ),
        quat-conj(q)
    ).slice(1, 4)
}

#let project(focal-length, point) = {
    let n = 1

    let orig-z = point.at(2)
    let w = -orig-z
    let (x, y) = vec-mul(point.slice(0, 2), focal-length / w)
    (x, y, n / w, 1 / w)
}

#let poly-normal(poly) = {
    let v0 = vec-sub(poly.verts.at(1), poly.verts.at(0))
    let v1 = vec-sub(poly.verts.at(2), poly.verts.at(0))
    let n = vec-cross(v0, v1)
    vec-unit(n)
}

#let is-in-front-plane(poly0, poly1) = {
    let poly1 = (original-verts: poly1.original-verts.map(p => vec-sub(p, poly0.original-verts.at(0))))
    poly1.original-verts.all(v => vec-dot(v, poly0.normal) < 0.0000001)
}
#let is-behind-plane(poly0, poly1) = {
    let poly1 = (original-verts: poly1.original-verts.map(p => vec-sub(p, poly0.original-verts.at(0))))
    poly1.original-verts.all(v => vec-dot(v, poly0.normal) > -0.0000001)
}

#let overlaps(min0, max0, min1, max1) = not (max0 - min1 < 0.0000001 or max1 - min0 < 0.0000001)

#let sat-project(axis, points) = {
    let max = -calc.inf
    let min = calc.inf
    for p in points {
        let proj = vec-dot(axis, p)
        min = calc.min(min, proj)
        max = calc.max(max, proj)
    }
    (min, max)
}

#let sat-collision(poly0, poly1) = {
    let axes = ()
    for i in range(poly0.verts.len()) {
        let (x, y) = vec-sub(poly0.verts.at(calc.rem(i + 1, poly0.verts.len())), poly0.verts.at(i)).slice(0, 2)
        axes.push(vec-unit((-y, x)))
    }
    for i in range(poly1.verts.len()) {
        let (x, y) = vec-sub(poly1.verts.at(calc.rem(i + 1, poly1.verts.len())), poly1.verts.at(i)).slice(0, 2)
        axes.push(vec-unit((-y, x)))
    }
    for axis in axes {
        let (min0, max0) = sat-project(axis, poly0.verts)
        let (min1, max1) = sat-project(axis, poly1.verts)
        if not overlaps(min0, max0, min1, max1) {
            return false
        }
    }
    true
}

#let line-intersect-or-almost(p0, v0, p1, v1) = {
    let (p0x, p0y) = p0; let (p1x, p1y) = p1
    let (v0x, v0y) = v0; let (v1x, v1y) = v1
    let dpx = p1x - p0x; let dpy = p1y - p0y

    let denom = v1x * v0y - v0x * v1y
    if calc.abs(denom) < 0.0000001 {
        return none
    }
    let mult = 1 / denom

    let t = (dpy * v1x - dpx * v1y) * mult
    let u = (dpy * v0x - dpx * v0y) * mult

    if t < -0.05 or t > 1.05 or u < -0.05 or u > 1.05 {
        return none
    }

    (t, u)
}

#let is-behind-lines(prim0, prim1) = {
    let intersected = false
    for i in range(prim0.verts.len() - if prim0.type == "path" { 1 } else { 0 }) {
        let p0 = prim0.verts.at(i)
        let q0 = prim0.verts.at(calc.rem(i + 1, prim0.verts.len()))
        let v0 = vec-sub(q0, p0).slice(0, 2)
        for j in range(prim1.verts.len() - if prim1.type == "path" { 1 } else { 0 }) {
            let p1 = prim1.verts.at(j)
            let q1 = prim1.verts.at(calc.rem(j + 1, prim1.verts.len()))
            let v1 = vec-sub(q1, p1).slice(0, 2)
            let intersect = line-intersect-or-almost(p0.slice(0, 2), v0, p1.slice(0, 2), v1)
            if intersect != none {
                intersected = true
                let (t, u) = intersect
                let d0 = (1 - t) * p0.at(2) + t * q0.at(2)
                let d1 = (1 - u) * p1.at(2) + u * q1.at(2)
                if t > 0.001 and t < 0.999 and u > 0.001 and u < 0.99 and calc.abs(d1 - d0) < 0.0001 {
                    panic("Intersecting polygons detected", t, u)
                }
                if d1 < d0 {
                    return false
                }
            }
        }
    }
    if prim0.type == "poly" and prim1.type == "poly" {
        intersected
    } else {
        true
    }
}

#let could-obscure(prim0, extent0, prim1, extent1) = {
    if not overlaps(extent0.xmin, extent0.xmax, extent1.xmin, extent1.xmax) { return false }
    if not overlaps(extent0.ymin, extent0.ymax, extent1.ymin, extent1.ymax) { return false }
    if prim0.type == "poly" and is-behind-plane(prim0, prim1) { return false }
    if prim1.type == "poly" and is-in-front-plane(prim1, prim0) { return false }
    if prim0.type == "poly" and prim1.type == "poly" and not sat-collision(prim0, prim1) { return false }
    if is-behind-lines(prim0, prim1) { return false }
    true
}

#let sort-prims(prims) = {
    prims = prims.sorted(
        key: p => -calc.min(..p.verts.map(p => p.at(2)))
    )
    prims = prims.map(p => (
        marked: false,
        ..p
    ))
    let extents = prims.map(p => (
        xmin: calc.min(..p.verts.map(p => p.at(0))),
        xmax: calc.max(..p.verts.map(p => p.at(0))),
        ymin: calc.min(..p.verts.map(p => p.at(1))),
        ymax: calc.max(..p.verts.map(p => p.at(1))),
        zmin: calc.min(..p.verts.map(p => p.at(2))),
        zmax: calc.max(..p.verts.map(p => p.at(2))),
    ))
    let ret = ()
    let _ = while prims.len() != 0 {
        let pp = prims.at(prims.len() - 1)
        let pe = extents.at(prims.len() - 1)
        let needs-insert = true
        for i in range(0, prims.len() - 1).rev() {
            let qp = prims.at(i)
            let qe = extents.at(i)
            if pe.zmax <= qe.zmin {
                if not qp.marked {
                    ret.push(pp); prims.pop(); extents.pop()
                    needs-insert = false
                    break
                }
            } else {
                if could-obscure(pp, pe, qp, qe) {
                    if could-obscure(qp, qe, pp, pe) {
                        //panic("work this out later", pe, qe)
                    }
                    if qp.marked {
                        //panic("work this out later")
                        ret.push(pp); prims.pop(); extents.pop()
                        needs-insert = false
                        break
                    }
                    qp.marked = true
                    prims.remove(i)
                    extents.remove(i)
                    prims.push(qp)
                    extents.push(qe)
                    needs-insert = false
                    break
                }
            }
        }
        if needs-insert {
            ret.push(pp); prims.pop(); extents.pop()
        }
    }
    ret
}

#let prim-map(prims, func) = prims.map(prim => {
    prim.verts = prim.verts.map(func)
    if "lines" in prim {
        prim.lines = prim.lines.map(
            l => if type(l) == "array" { l.map(func) } else { (..l, points: l.points.map(func)) }
        )
    }
    prim
})

#let map-if-not-index(func) = point => if type(point) == "array" { func(point) } else { point }
#let obj-points-map(obj, func) = (
    points: obj.at("points", default: ()).map(func),
    polygons: prim-map(obj.at("polygons", default: ()), map-if-not-index(func)),
    paths: prim-map(obj.at("paths", default: ()), map-if-not-index(func)),
)

#let deindex(obj) = (
    prim-map(obj.polygons, p => if type(p) == "array" { p } else { obj.points.at(p) }).map(p => (type: "poly", ..p))
    + prim-map(obj.paths, p => if type(p) == "array" { p } else { obj.points.at(p) }).map(p => (type: "path", ..p))
)

#let render(
    width: auto,
    height: auto,
    cam-loc: none,
    cam-rot: none,
    unit-length: none,
    focal-length: none,
    line-thickness: 0.2pt,
    clip: true,
    attachments: (),
    ..objects
) = {
    assert(width == auto or type(width) == "length" or type(width) == "ratio" or type(width) == "relative length")
    assert(height == auto or type(height) == "length" or type(height) == "ratio" or type(height) == "relative length")
    assert(cam-loc == none or (type(cam-loc) == "array" and cam-loc.len() == 3))
    assert(cam-rot == none or (type(cam-rot) == "array" and cam-rot.len() == 4))
    assert(type(unit-length) == "length")
    assert(type(focal-length) == "integer" or type(focal-length) == "float")

    let prims = ()
    for obj in objects.pos() {
        let inner-obj = obj-points-map(obj, v => vec-elemprod(v, obj.at("scale", default: (1, 1, 1))))
        if "rot" in obj {
            inner-obj = obj-points-map(inner-obj, p => quat-rot(obj.rot, p))
        }
        inner-obj = obj-points-map(inner-obj, v => vec-add(v, obj.at("loc", default: (0, 0, 0))))
        if cam-loc != none {
            inner-obj = obj-points-map(inner-obj, v => vec-sub(v, cam-loc))
        }
        if cam-rot != none {
            inner-obj = obj-points-map(inner-obj, p => quat-rot(quat-conj(cam-rot), p))
        }
        for p in deindex(inner-obj) {
            prims.push(p)
        }
    }
    prims = prims.map(p => if p.type == "poly" {
        (
            ..p,
            original-verts: p.verts,
            normal: poly-normal(p),
        )
    } else {
        (
            ..p,
            original-verts: p.verts,
        )
    })

    prims = prims.filter(prim => if "normal" in prim { -vec-dot(prim.verts.at(0), prim.normal) > 0.0000001 } else { true })
    prims = prim-map(prims, p => project(focal-length, p))
    prims = sort-prims(prims)
    prims = prim-map(prims, ((x, y, z, w)) => (x, -y, z, w))
    let box-width = if width == auto {
        let min = calc.min(..prims.map(prim => prim.verts.map(p => p.at(0))).flatten())
        (
            calc.max(..prims.map(prim => prim.verts.map(p => p.at(0))).flatten()) - min
        ) * unit-length
    } else {
        width
    }
    let box-height = if height == auto {
        let min = calc.min(..prims.map(prim => prim.verts.map(p => p.at(1))).flatten())
        (
            calc.max(..prims.map(prim => prim.verts.map(p => p.at(1))).flatten()) - min
        ) * unit-length
    } else {
        height
    }

    box(
        width: box-width,
        height: box-height,
        clip: clip,
        layout(size => {
            let basex = if width == auto {
                let min = calc.min(..prims.map(prim => prim.verts.map(p => p.at(0))).flatten())
                -min * unit-length
            } else {
                size.width / 2
            }
            let basey = if height == auto {
                let min = calc.min(..prims.map(prim => prim.verts.map(p => p.at(1))).flatten())
                -min * unit-length
            } else {
                size.height / 2
            }
            let prims = prim-map(
                prims,
                ((x, y, z, w)) => (
                    x * unit-length + basex,
                    y * unit-length + basey,
                )
            )
            for prim in prims {
                if prim.type == "poly" {
                    place(top + left, polygon(
                        fill: prim.fill-color,
                        stroke: if prim.at("outlined", default: true) {
                            prim.edge-color + prim.at("line-thickness", default: line-thickness)
                        } else {
                            0pt
                        },
                        ..prim.verts
                    ))
                    for l in prim.at("lines", default: ()) {
                        let l = if type(l) == "array" { (points: l) } else { l }
                        place(top + left, line(
                            stroke: l.at("stroke", default: prim.edge-color + line-thickness),
                            start: l.points.at(0),
                            end: l.points.at(1),
                        ))
                    }
                } else {
                    let closed = prim.verts.at(0) == prim.verts.at(prim.verts.len() - 1)
                    place(top + left, path(
                        stroke: prim.stroke,
                        closed: closed,
                        ..prim.verts
                    ))
                }
            }
            for obj in objects.pos() {
                for att in obj.at("attachments", default: ()) {
                    let loc = att.loc
                    loc = if type(loc) == "array" { loc } else { obj.points.at(loc) }
                    loc = vec-elemprod(loc, obj.at("scale", default: (1, 1, 1)))
                    if "rot" in obj {
                        loc = quat-rot(obj.rot, loc)
                    }
                    loc = vec-add(loc, obj.at("loc", default: (0, 0, 0)))
                    if cam-loc != none {
                        loc = vec-sub(loc, cam-loc)
                    }
                    if cam-rot != none {
                        loc = quat-rot(quat-conj(cam-rot), loc)
                    }
                    loc = project(focal-length, loc)
                    let (x, y, z, w) = loc
                    place(top + left, dx: x * unit-length + basex, dy: -y * unit-length + basey, box(att.content))
                }
            }
        })
    )
}
