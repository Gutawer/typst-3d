#import "typst-3d.typ": *

#set page(margin: 1cm)

// Function which makes a unit cube centered around the origin with 6 unique side colors.
#let make-cube(xn, yn) = {
    let polys = ()
    for (axis, angle, color) in (
        ((0, 1, 0),   0deg, blue),
        ((0, 1, 0),  90deg, red),
        ((0, 1, 0), 180deg, purple),
        ((0, 1, 0), 270deg, green),
        ((1, 0, 0),  90deg, yellow),
        ((1, 0, 0), -90deg, orange),
    ) {
        let q = quat(axis: axis, angle: angle)
        polys.push((
            edge-color: color,
            fill-color: color.lighten(80%),
            verts: (
                (-0.5, -0.5, 0.5),
                ( 0.5, -0.5, 0.5),
                ( 0.5,  0.5, 0.5),
                (-0.5,  0.5, 0.5),
            ).map(p => quat-rot(q, p)).map(p => p.map(n => calc.round(n, digits: 1))),
            lines: (
                range(1, xn).map(x => (
                    (-0.5 + x / xn, -0.5, 0.5),
                    (-0.5 + x / xn,  0.5, 0.5),
                ))
                + range(1, yn).map(y => (
                    (-0.5, -0.5 + y / yn, 0.5),
                    ( 0.5, -0.5 + y / yn, 0.5),
                ))
            ).map(l => l.map(p => quat-rot(q, p))),
        ))
    }
    polys
}

#let cube = make-cube(2, 2)
// `torus.json` is generated with some basic Blender Python scripting:
// ```py
// import bpy
// import json
//
// obj = bpy.context.active_object
//
// out = {
//     "points": [],
//     "polygons": []
// }
// for v in obj.data.vertices:
//     out["points"].append(list(v.co))
// for f in obj.data.polygons:
//     verts = []
//     for idx in f.vertices:
//         verts.append(idx)
//     out["polygons"].append({ "verts": verts })
// out["loc"] = list(obj.location)
// last_mode = obj.rotation_mode
// obj.rotation_mode = "QUATERNION"
// out["rot"] = list(obj.rotation_quaternion)
// obj.rotation_mode = last_mode
// out["scale"] = list(obj.scale)
//
// with open("<PATH TO FOLDER>/torus.json", "w") as f:
//     json.dump(out, f)
// ```
// This exports the object as a `.json` file which is then given a color by the lines below.
// To use it, you'd have to replace `<PATH TO FOLDER>` with a suitable path for you.
#let torus = {
    let res = json("torus.json")
    res.polygons = res.polygons.map(p => (
        edge-color: blue,
        fill-color: blue.lighten(80%),
        ..p,
    )).map(p => {
        if "lines" in p {
            p.lines = p.lines.map(
                l => if type(l) == "array" { l } else { (..l, stroke: (paint: rgb(l.color), thickness: 0.6pt, cap: "round")) }
            )
        }
        p
    })
    res
}

#align(center, box(stroke: purple.darken(20%) + 1pt, inset: 0.5pt, radius: 4pt, fill: white, render(
    // These are arbitrarily chosen.
    width: 300pt, height: 200pt,

    // The coordinate system used by the API is the same as the one Blender uses, because you can
    // therefore use Blender to export scenes for rendering in the PDF. In particular, +X is right,
    // +Y is up, and +Z is "coming out of the screen". Just like Blender, a camera without any
    // transform is facing downwards at (0, 0, 0). Because of this coordinate protocol, transforms
    // can be copied directly from a Blender scene. Getting the camera to match a Blender camera
    // directly involves a little work.
    //
    // `unit-length` is half of the width, corresponding to a coordinate system where -1 == left
    // boundary, +1 == right boundary, 0 == middle, which is pretty standard in computer graphics.
    // Keep in mind that Blender defines cameras in terms of the maximum width/height value. In
    // particular, if `height > width`, this would be `height / 2` instead, and therefore -1 and 1
    // coordinate boundaries would instead be the vertical ones.
    unit-length: 300pt / 2,

    // This is converted from blender, which uses a camera-inspired interface with two variables -
    // focal length in mm, and sensor size in mm. Dividing them and multiplying by the width of the
    // image gives the focal length expected by the API.
    //
    // Alternatively, you could use Field of View, which maps to focal length in the following way:
    // `focal-length: 1 / calc.tan(45.90817307525696deg / 2)` Note that if you're using the Blender
    // `unit-length` protocol above, FOV is intuitively actually either horizontal or vertical
    // depending on whether `width` or `height` is bigger. Here it's horizontal FOV, because
    // `width > height`.
    focal-length: 2 * 42.5 / 36,

    // This is exported from a Blender camera, and the values aren't particularly meaningful.
    // I could have also copied the quaternion values directly.
    cam-rot: quat(
        axis: (0.857059, 0.292298, 0.424278),
        angle: 69.6deg,
    ),
    // Same here.
    cam-loc: (2.11344, -3.00871, 1.77553),

    // The torus is placed directly with no transform.
    torus,

    // The cube is scaled a bit smaller and given two rotations so that it shows unique
    // colors compared to the torus, and so that it is rotated 45 degrees on the horizontal
    // plane. Keep in mind that quaternion multiplication order means that later values
    // are applied first!
    (
        polygons: cube,
        paths: (
            (
                stroke: olive,
                verts: (
                    (-1.0,  1.4,  -1.4 ),
                    (-1.0, -1.4,   1.4 ),
                    (-0.8, -1.53,  1.53),
                    ( 0.8, -1.7,   1.7),
                    ( 1.0, -1.5,   1.5),
                    ( 1.0,  1.5,  -1.5),
                    ( 0.8,  1.7,  -1.7),
                    (-0.8,  1.53, -1.53),
                    (-1.0,  1.4,  -1.4 ),
                )
            ),
        ),
        scale: (0.7, 0.7, 0.7),
        rot: quat-mul(
            quat(
                axis: (0, 0, 1),
                angle: 45deg,
            ),
            quat(
                axis: (0, 1, 0),
                angle: 90deg,
            ),
        ),
        attachments: (
            (
                loc: (-0.5, 0, 0),
                content: style(styles => {
                    let content = {
                        let col = maroon
                        let t = box(
                            fill: white, inset: 1pt, radius: (top-left: 1.5pt, top-right: 1.5pt),
                            text(size: 8pt, fill: col, font: "DejaVu Sans Mono", "Cube")
                        )
                        let size = measure(t, styles)
                        place(dx: 5pt, dy: -4pt - size.height, t)
                        place(path(
                            stroke: (thickness: 0.5pt, paint: col, cap: "round"),
                            (0pt, 0pt), (4pt, -4pt), (4pt + size.width + 1.5pt, -4pt)
                        ))
                        place(dx: -1pt, dy: -1pt, circle(radius: 1pt, fill: col))
                    }
                    content
                })
            ),
            (
                loc: (-0.5, -0.5, 0.5),
                content: style(styles => {
                    let content = {
                        let col = orange.darken(10%)
                        let t = box(
                            fill: white, inset: 1pt, radius: (top-left: 1.5pt, top-right: 1.5pt),
                            text(size: 8pt, fill: col, font: "DejaVu Sans Mono", "Corner of Cube")
                        )
                        let size = measure(t, styles)
                        place(dx: 5pt, dy: -4pt - size.height, t)
                        place(path(
                            stroke: (thickness: 0.5pt, paint: col, cap: "round"),
                            (0pt, 0pt), (4pt, -4pt), (4pt + size.width + 1.5pt, -4pt)
                        ))
                        place(dx: -1pt, dy: -1pt, circle(radius: 1pt, fill: col))
                    }
                    content
                })
            ),
        ),
    )
)))

// This function places a grid of 25 pairs of cubes onto the page, where each one
// is rotating around an axis more and more each image, until a full rotation is complete.
#let render-cubes(w, h, xn, yn) = {
    let is = 25
    align(center)[#grid(columns: 5, gutter: 5pt,
        ..range(is).map(i => {
            let polys = make-cube(xn, yn)
            box(stroke: purple.darken(20%) + 1pt, inset: 0.5pt, radius: 4pt, fill: white, align(center, render(
                width: w, height: h, unit-length: 2em,
                focal-length: 10.0,
                (
                    rot: quat(
                        axis: (1, 2, 0),
                        angle: i * 360deg / (is - 1),
                    ),
                    loc: (0.3, -0.50, -5),
                    scale: (0.6, 0.6, 0.6),
                    polygons: polys
                ),
                (
                    rot: quat(
                        axis: (4, -1, 0),
                        angle: i * 360deg / (is - 1),
                    ),
                    loc: (-0.1, 0.20, -3.5),
                    scale: (0.6, -0.6, -0.6),
                    polygons: polys
                ),
            )))
        })
    )]
}

#render-cubes(100pt, 100pt, 3, 3)
