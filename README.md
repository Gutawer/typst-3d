# Typst 3D Rendering

This is a basic library for 3D rendering within the [Typst typesetting system](https://github.com/typst/typst), capable of outputting vector graphic 3D renderings of polygon meshes.

![An image containg a torus surrounding a square, and then 3 renderings of rotating pairs of cubes underneath](./example.png){width=80%}

### Example Usage

See the `example.typ` file for example usage of the API, including examples on how to render the images shown here on the README.

## Functions

### `render` Function

The `render` function is the entry point into the library. It takes in the following named arguments:

- **`width`** _(`auto` or `relative length`)_ \
  The width of the resulting `box`. If `auto`, it will become as wide as the rendered object.
- **`height`** _(`auto` or `relative length`)_ \
  The height of the resulting `box`. If `auto`, it will become as tall as the rendered object.
- **`cam-loc`** _(`none` or `array` of three `float`s)_ \
  The position of the camera viewing the scene, in XYZ order. If `none`, apply no translation (equal to a translation of `(0, 0, 0)`).
- **`cam-rot`** _(`array` of four `float`s)_ \
  A quaternion defining the camera's orientation, in `WXYZ` order with `W` being the real component. If `none`, apply no rotation (equal to a rotation of `(1, 0, 0, 0)`).
- **`unit-length`** _(`length`)_ \
  The amount one rendered unit corresponds to in the resulting document. The value of this is context-dependent, and you can see some discussion about it in the `example.typ` file.
- **`focal-length`** _(`float`)_ \
  A scale factor on the rendered object, allowing for easier matching of a known scene. Again see `example.typ` for some discussion on how to choose this value, including how to convert to it from Field of View (FOV).
- **`line-thickness`** _(`relative length`)_ \
  How thick to draw the lines in the render. \
  Default: `0.2pt`
- **`clip`** _(`boolean`, default `true`)_ \
  Whether to clip the rendered context inside the box it's rendered within.
- **`..objects`** _(variadic list of `dict`s)_ \
  The objects to render. Each object is a dictionary potentially containing:
  - **`rot`** _(`array` of four `float`s)_ \
    The rotation of this object as a quaternion in WXYZ order. If missing, defaults to no rotation.
  - **`loc`** _(`array` of three `float`s)_ \
    The translation of this object in XYZ order. If missing, defaults to no translation.
  - **`scale`** _(`array` of three `float`s)_ \
    The scale of this object, as per-axis scale factors. If missing, defaults to no scaling (equal to a scale of `(1, 1, 1)`).
  - **`points`** _(`array` of `array` of three floats)_ \
    The points to use for indexed rendering.
  - **`polygons`** _(`array` of `dict`s)_ \
    The polygons in this object. Each polygon is a dictionary containing:
    - **`edge-color`** _(`color`)_ \
      The color to draw this polygon's edges with.
    - **`fill-color`** _(`color`)_ \
      The color to draw this polygon's fill with.
    - **`verts`** _(`array` of (`integer` or `array` of three `float`s))_ \
      The vertices in this polygon, stored as an array of vertices. Each vertex is either an array of three numbers in XYZ order, or an index into the object's `points` array.
    - **`lines`** _(`array` of `line`s)_ \
      A set of lines to draw when this polygon is drawn. Much like `verts`, these can either be specified as pairs of points or indices.
      A `line` is either an array of two points, or a dictionary containing `points` (an `array` of two points) and `stroke`, a typst stroke value.
  - **`paths`** _(`array` of `dict`s)_ \
    The paths in this object. Each path is a dictionary containing:
    - **`stroke`** _(typst stroke values)_ \
      The stroke to use for the path.
    - **`verts`** _(`array` of (`integer` or `array` of three `float`s))_ \
      The vertices in this path, stored as an array of vertices. Each vertex is either an array of three numbers in XYZ order, or an index into the object's `points` array.
  - **`attachments`** _(`array` of `dict`s)_ \
    Overlaid content to be displayed using this object's transformation. Each attachment is a dictionary containing:
    - **`loc`** _(`array` of three `float`s)_ \
      The location in this object's space to draw the content at.
    - **`content`** _(`content`)_ \
      The content to draw.

The coordinate system used is the one used by Blender, where `+X` is right, `+Y` is up and `+Z` is "out of the screen". Therefore, an object at depth `1` should be positioned at `Z = -1`.

### `quat` and `quat-mul` Functions

Using quaternions directly is extremely unwieldy. Instead, to use the API, I advise that you use the `quat` function, which takes in an `axis` named argument and an `angle` named argument. To concatenate two quaternion rotations, you can use the `quat-mul` function, but keep in mind that quaternions work in right-to-left order. That is, `quat-mul(q1, q0)` will apply `q0` first, then `q1`. You can see examples of these functions in `example.typ`.
